
# Log Analyser
A programming project by Wiebke Dirks. This tool is for analysing logging files. It extracts Points of Interests, ie. errors or exceptions
from a file and saves them.

# REQUIREMENTS
In order to compile and run, one requires java developer tools (i.e. `javac`, e.g. provided by `openjdk-jdk`) and a java runtime (i.e. `java`, e.g. `openjdk-jre`).
The program names may slightly differ between operating systems. Tested and thus recommended version number for both tools is 8.

#BUILD
If necessary the application can be build by running
`mvn package`
from the terminal from the folder where this document is located.
Make sure `Maven` is installed for this.

#RUNNING TESTS
To run the automated test run
`mvn test`
from the terminal from the folder where this document is located.
Make sure `Maven` is installed for this.


# EXECUTION
From the folder where this document is located, call the following command from a terminal


`java -jar target/loggingtool-jar-with-dependencies.jar -help`

to see available options.

# FLAGS
In order to set their analysation parameters, one is required to use a combination of the following flags.
If no flags are provided, the log analyser is started in GUI mode

# SYNOPSIS
Explanation:
- Expressions within square brackets [] are optional.
- Expressions within braces {} are mandatory.
- Expressions separated by a pipe | are mutually exclusive (XOR).
- Expression separated by ^ behave after the fashion of the logical inclusive OR.


```bash
java -jar target/loggingtool-jar-with-dependencies.jar
  [
    {
      -p Filepaths -target OutputLocation -out single|sep|fw|screen (-exc ^ -err)
    }
    [-b INTEGER] [-a INTEGER] [-help]
  ]
```

# OPTIONS

| FLAG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               | PARAMETER            | DESCRIPTION                                   | 
| :----------------- | :------------------- | :-------------------------------------------- |
| `-help`            | |prints all available options to terminal |
| `-p`               | Filepaths |Filepaths to log files (from directory application is started in) multiple entries to be separated by blank space |
| `-target`          | Filepath|Path to directory where output is to be stored |
| `-out`             | |Output Options | 
|                    | `single`         | All Points of Interests (POI) stored in single file |
|                    | `sep`            | All POIs grouped and stored by type. New file for each type. |
|                    | `fw`             | All POIs from one log file stored in one output file. new output file for each input file|
|                    | `screen`         | All POIs are printed to the terminal|
| `-exc`             | |Extracts all EXCEPTION Pois, has to be set if -err is not set. |
| `-err`             | |Extracts all ERROR Pois, has to be set if -exc is not set |
| `-b`               | INTEGER | Number of lines of log file that is to be added to output file before occurrence of a POI |
| `-a`               | INTEGER | Number of lines of log file that is to be added to output file after occurrence of a POI |

# EXAMPLES

| COMMAND | RESULT |
| :--- | :--- |
| java -jar target/loggingtool-jar-with-dependencies.jar -target output/ - p log/log.txt -err -single | Extracts all unique errors from log.txt and saves them to one file in output|
| java -jar target/loggingtool-jar-with-dependencies.jar -target output/ - p log/log1.txt log/log2.txt -err -exc - a 3 -sep | Extracts all unique errors and exceptions from log1.txt and log2.txt. Errors and Exceptions are saved in separate files in output |
| java -jar target/loggingtool-jar-with-dependencies.jar | starts a simplified version of the application that allows the user to use a GUI for input|

# Algorithm for finding POIs
To prevent the same POIs to be saved repeatedly the POIs are analysed in terms of length and editing distance (Levenshtein distance) to minimise duplicates.