package loggingtool.patterns;

/**
 * Wrapper class for POIs to compare them to each other, eg to reduce duplicates.
 */
public class PoiFuzzyEqualityWrapper {
    /**
     * Poi
     */
    private final Poi poi;

    /**
     * Constructor.
     * @param poi to be compared
     */
    public PoiFuzzyEqualityWrapper(Poi poi) {
        if (poi == null) {
            throw new IllegalArgumentException();
        }
        this.poi = poi;
    }

    /**
     * Had to override hashcode. default value.
     * @return default value
     */
    @Override
    public int hashCode() {
        return 42;
    }
    //TODO find a better algorithm, enable the user to decide how strict the analysis should be
    /**
     * Compares two pois to see if they are the same.
     *
     * Two wrapped POIs are compared in terms of length of the line and the editing distance.
     *
     * @param o Object to be compared to
     * @return wheather the objects are the same
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PoiFuzzyEqualityWrapper)) {
            return false;
        }
        final PoiFuzzyEqualityWrapper that = (PoiFuzzyEqualityWrapper) o;


        if (Math.min(this.poi.getName().length(), that.poi.getName().length()) < 0.9 *
                Math.max(this.poi.getName().length(), that.poi.getName().length())) {
            return false;
        } else {
            int distance = EditingDistance.getEditingDistance(this.poi.getName(), that.poi.getName());
            return distance < 30;
        }
    }

    public Poi getPoi() {
        return poi;
    }
}
