package loggingtool.patterns;

import java.util.ArrayList;

/**
 * Represents a POI.
 * <p>
 * Each POI contains data about in which file and at which line it was found
 * and the lines that are part of the POI.
 */
public class Poi {
    /**
     * File from which POI is extracted.
     */
    private final String sourceFile;
    /**
     * Line at which POI was found.
     */
    private final int startLine;
    /**
     * Type of POI
     */
    private final String type;
    /**
     * Lines that make up the POI.
     */
    private ArrayList<String> log;

    private String name;

    /**
     * Constructor
     *
     * @param sourceFile File from which POI is extracted.
     * @param startLine  Line at which POI was found.
     * @param log        Lines that make up the POI.
     * @param type       Type of POI
     */
    public Poi(final String name, final String sourceFile, final int startLine, ArrayList<String> log, String type) {
        this.sourceFile = sourceFile;
        this.startLine = startLine;
        this.log = log;
        this.type = type;
        this.name = name;
    }

    /**
     * Getter for File from which POI is extracted
     * @return full path to file
     *
     */
    private String getSourceFile() {
        return sourceFile;
    }

    /**
     * Getter for line POI was found
     * @return number of line
     */
    public int getStartLine() {
        return startLine;
    }

    /**
     * Getter for type of POI (error, exception)
     * @return name of type
     */
    public String getType() {
        return type;
    }

    /**
     * Returns a String that contains the metadata and the lines
     * that make up the POI itself.
     *
     * @return String with data of POI
     */
    public String getStringRepresentationOfPoi() {

        StringBuilder sb = new StringBuilder();
        sb.append("Input file: ")
                .append(getSourceFile())
                .append("\n").append(getType())
                .append(" found at line: ")
                .append(getStartLine())
                .append("\n");

        for (String line : log) {
            sb.append(line)
                    .append("\n");
        }
        return sb.toString();
    }

    /**
     * Gets the content of the line where the POI was found
     * @return content of line
     */
    public String getName() {
        return name;

    }
}
