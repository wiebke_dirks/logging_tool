package loggingtool.patterns;


import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Ensures that all POIs are unique, eg if two POIs are too similar, only one is kopt
 */
public class UniquenessEvaluator {

    /**
     * Deletes all duplicate POIs from a list.
     */
    public static List<Poi> getUniquePois(List<Poi> pois) {

        Set<PoiFuzzyEqualityWrapper> uniquePois = new HashSet<>();
        for (Poi poi : pois) {
            PoiFuzzyEqualityWrapper wrapper = new PoiFuzzyEqualityWrapper(poi);

            uniquePois.add(wrapper);
        }

        return uniquePois.stream()                    // For all the unique pois wrapped ...
                .map(PoiFuzzyEqualityWrapper::getPoi) // ... get the pois stored in it ...
                .collect(Collectors.toList());         // ... and collect them to a list.
    }
}
