package loggingtool.validation;

import loggingtool.setup.Configuration;

import java.io.File;

public class InputValidation {

    private Configuration config;

    public InputValidation(Configuration config) {
        if (config == null) {
            throw new IllegalArgumentException();
        }
        this.config = config;
    }

    public boolean checkUsage() {

        if (config.getAfter() < 0) {
            System.err.println("Value for after must be positive.");
            return false;
        }
        if (config.getBefore() < 0) {
            System.err.println("Value for before must be positive.");
            return false;
        }
        if (config.getTargetFolder() == null) {
            System.err.println("Output folder does not exist");
            return false;
        }
        if (!config.getWantErrors() && !config.getWantExceptions()) {
            System.err.println("You did not specify what criteria you want to analyse.");
            return false;
        }
        if (config.getSaveOption() == null || config.getSaveOption().equals("")) {
            return false;
        }
        if (!config.getSaveOption().equals("screen") &&
                !config.getSaveOption().equals("fw") &&
                !config.getSaveOption().equals("sep") &&
                !config.getSaveOption().equals("single")) {
            System.err.println("You did not specify how you would like to store the output.");
            return false;
        }

        if (config.getInputFiles() == null) {
            System.err.println("You did not specify input files");
            return false;
        } else {
            for (String file : config.getInputFiles()) {

                if (!new File(file).exists()) {
                    System.err.println("Input file does not exist");
                    return false;
                }
            }
        }


        return true;


    }

}
