package loggingtool;

import loggingtool.fileManagement.FileAnalyser;
import loggingtool.fileSaving.SaveFactory;
import loggingtool.fileSaving.Saver;
import loggingtool.patterns.Poi;
import loggingtool.patterns.UniquenessEvaluator;
import loggingtool.setup.Configuration;
import loggingtool.validation.InputValidation;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

/**
 * After setting up the Configuration this class directs the further processes.
 */
public class AnalysationDirector {

    /**
     * Uses configuration to direct further processes.
     *
     * Checks if user input is valid.
     * Analyses logging files according to the specification of a user.
     * Extracts Points Of Interests (POI) from theses files.
     * Organises these POIs and saves them in text files or prints them to Standard Output.
     *
     * @param config Configuration that user specified via the command line or GUI
     */
    public static void doAnalysation(Configuration config) {
        InputValidation validator = new InputValidation(config);

        // checks whether the Configuration according to user input is valid.
        if (!validator.checkUsage()) {
            System.err.println(("Try \"--help\" option to get list of available options."));
            System.exit(2);
        }


        SaveFactory saveFactory = new SaveFactory();
        Saver saver = saveFactory.getSaver(config);


        /*
         * extracts the POIs for every Input File given and analyses them
         * according to criteria given and saves them.
         */
        for (final String path : config.getInputFiles()) {
            FileAnalyser analyser = new FileAnalyser(Paths.get(path));
            try {

                List<Poi> findings = analyser.getAllPoisFromFile(config, path);
                Utility.sort(findings);
                saver.save(UniquenessEvaluator.getUniquePois(findings));
            } catch (IOException e) {
                System.err.println("An IO error occurred");
                System.err.println(("Try \"--help\" option to get list of available options."));
                System.exit(3);
            }
        }
    }
}
