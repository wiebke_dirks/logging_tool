package loggingtool.setup;

public class ConfigException extends Exception {

    public ConfigException() {
        super();
    }

    public ConfigException(String s) {
        super(s);
    }

    public ConfigException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ConfigException(Throwable throwable) {
        super(throwable);
    }

}
