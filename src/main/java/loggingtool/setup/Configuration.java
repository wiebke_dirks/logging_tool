package loggingtool.setup;

import loggingtool.detectors.DetectorFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Stores the Configuration for the logging tool according to user input.
 */
public class Configuration {

    /**
     * wantException is true if user wishes that exceptions are analysed
     */
    private boolean wantExceptions;
    /**
     * wantErrors is true if user wishes that exceptions are analysed
     */
    private boolean wantErrors;
    /**
     * Options for saving: Filewise, Single File, Separate Files, Screen
     */
    private String saveOption;
    /**
     * Paths to files that user wants analysed
     */
    private Set<String> inputFiles;

    /**
     * Number of lines before a POI that a user wants displayed.
     */
    private int before;
    /**
     * Number of lines after a POI that a user wants displayed.
     */
    private int after;
    /**
     * Output is stored in this folder.
     */
    private String targetFolder;


    public Set<String> getInputFiles() {
        return inputFiles;
    }


    public void setInputFiles(Set<String> inputFiles) {
        this.inputFiles = inputFiles;
    }

    public String getSaveOption() {
        return saveOption;
    }


    public void setSaveOption(String saveOption) {
        this.saveOption = saveOption;
    }

    public String getTargetFolder() {
        return targetFolder;
    }

    public void setTargetFolder(String targetFolder) {
        this.targetFolder = targetFolder;
    }

    public boolean getWantErrors() {
        return wantErrors;
    }

    public void setWantErrors(boolean wantErrors) {
        this.wantErrors = wantErrors;
    }

    public DetectorFactory.DetectorType[] getWantedDetectorTypes() {
        final List<DetectorFactory.DetectorType> detectors = new LinkedList<>();

        if (getWantErrors()) {
            detectors.add(DetectorFactory.DetectorType.ERROR);
        }
        if (getWantExceptions()) {
            detectors.add(DetectorFactory.DetectorType.EXCEPTION);
        }
        return detectors.toArray(new DetectorFactory.DetectorType[0]);
    }


    public boolean getWantExceptions() {
        return wantExceptions;
    }

    public void setWantExceptions(boolean wantExceptions) {
        this.wantExceptions = wantExceptions;
    }


    public int getBefore() {
        return before;
    }

    public void setBefore(int before) {
        this.before = before;
    }

    public int getAfter() {
        return after;
    }

    public void setAfter(int after) {
        this.after = after;
    }
}
