package loggingtool.setup;

import org.apache.commons.cli.*;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Parses user input.
 */
public class ArgumentParser {
    /**
     * All possible options a user can specify
     */
    private Options options;

    /**
     * Constructor.
     * <p>
     * Adds all possible options for user input.
     */
    public ArgumentParser() {

        options = new Options();
        options.addOption("h", "help", false, "prints all available options to terminal");
        options.addOption("b", "before", true, "INTEGER | Number of lines of log file that is to be added to output file before occurrence of a POI");
        options.addOption("a", "after", true, "INTEGER | Number of lines of log file that is to be added to output file after occurrence of a POI");
        options.addOption("target", "targetFolder", true, "Filepath|Path to directory where output is to be stored");
        options.addOption("exc", "exception", false, "Extracts all EXCEPTION POIs, has to be set if -err is not set.");
        options.addOption("err", "error", false, "Extracts all ERROR Pois, has to be set if -exc is not set");
        options.addOption("out", "outputFormat", true, "sep: All POIs grouped and stored by type. New file for each type.\n" +
                " single: All Points of Interests (POI) stored in single file\n" +
                " fw: All POIs from one log file stored in one output file. new output file for each input file\n"+
                "screen: All POIs are printed to the terminal");

        Option option = new Option("p", "filepath", true, "Filepaths |Filepaths to log files (from directory application is started in) multiple entries to be separated by blank space");
        option.setArgs(Option.UNLIMITED_VALUES);
        options.addOption(option);

    }

    /**
     * Gets all possible options for user input.
     *
     * @return options
     */
    private Options getOptions() {
        return options;
    }

    /**
     * Parses the user input.
     * <p>
     * Creates new Configuration object and sets its fields according to user input.
     * No validation of input happens here.
     * If user didn't specify: Before and After set to default 0.
     *
     * @param args user input
     * @return Configuration object that is configured according to user input
     * @throws ConfigException if input could not be parsed
     */
    public Configuration parseInput(String[] args) throws ConfigException {
        Configuration config = new Configuration();
        //Create a parser
        CommandLineParser parser = new DefaultParser();

        //parse the options passed as command line arguments
        try {
            Options options = getOptions();
            CommandLine cmd = parser.parse(options, args);
            //display all option if user specified help
            if (cmd.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("available input options:", options);
            }

            config.setWantExceptions(cmd.hasOption("exc"));
            config.setWantErrors(cmd.hasOption("err"));
            config.setSaveOption(cmd.getOptionValue("out"));


            config.setTargetFolder(cmd.getOptionValue("target"));


            if (cmd.hasOption("p")) {
                HashSet<String> files = new HashSet<>(Arrays.asList(cmd.getOptionValues("p")));
                config.setInputFiles(files);
            } else {
                config.setInputFiles(null);
            }

            if (cmd.hasOption("b")) {
                String bValue = cmd.getOptionValue("b");
                try {
                    config.setBefore(Integer.parseInt(bValue));
                } catch (NumberFormatException e) {
                    throw new ConfigException("Integer value expected for option 'b', but found '" + bValue + "'", e);
                }
            } else {
                //set default
                config.setBefore((0));
            }

            if (cmd.hasOption("a")) {
                String aValue = cmd.getOptionValue("a");
                try {
                    config.setAfter(Integer.parseInt(cmd.getOptionValue("a")));
                } catch (NumberFormatException e) {
                    throw new ConfigException("Integer value expected for option 'b', but found '" + aValue + "'", e);
                }
            } else {
                //set default
                config.setAfter(0);
            }
        } catch (org.apache.commons.cli.ParseException e) {
            throw new ConfigException("Input could not be parsed", e);
        }
        return config;
    }

}
