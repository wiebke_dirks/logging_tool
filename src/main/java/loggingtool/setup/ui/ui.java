package loggingtool.setup.ui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import loggingtool.AnalysationDirector;
import loggingtool.setup.Configuration;

import java.util.Arrays;
import java.util.HashSet;

/**
 * GUI that is started if user started application with no arguments.
 *
 * Prompts user with a graphical interface to assist user to specify analysation parameters.
 */
public class ui extends Application {
    private Stage window;
    private Button button;
    private Button help;
    private String[] userInputFile;
    private String userTarget;
    private String userSaveOpt;
    private boolean userError;
    private boolean userExc;
    private int userAfter;
    private int userBefore;
    private String helpMessage =

                    "Available input options:\n\n" +
                    "Input File:\n" +
                            "Filepaths to log files (from directory application is started in) multiple entries to be separated by blank space\n " +
                    "Output location:\n" +
                            "Specify  fielpath of folder in which you would like to save the ouput\n" +
                    "Number of lines before Point of Interest:\n" +
                            "display x lines before Point of Interest\n" +
                    "Number of lines before Point of Interest:\n" +
                            "display x lines before Point of Interest\n" +
                    "Type of Detector:\n" +
                            "Choose one or more analysation criterias\n" +
                    "Save option:             Specify how the output files should be grouped\n" +
                    "        separate:        Points of Interest grouped according to type, new file for each type\n" +
                    "        fw: filewise.    create new output file for each input file\n" +
                    "        single:          all Points of Interest of all files outputted in corresponding file\n" +
                    "        screen:          all Points of Interest are printed to Standard output";
    /**
     * Starts the GUI and parses user input.
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        window = primaryStage;
        window.setTitle("Log analyser");

        window.setOnCloseRequest(e -> {
            e.consume();
            closeProgram();
        });

        //GridPane with 10px padding around edge
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(5);


        //Name Label - constrains use (child, column, row)
        Label inputFilesLabel = new Label("Input Files:");
        GridPane.setConstraints(inputFilesLabel, 0, 0);

        TextField inputFiles = new TextField();
        inputFiles.setPromptText("eg.: /user/files/text.log");
        GridPane.setConstraints(inputFiles, 1, 0);

        Label targetLocationLabel = new Label("Output location:");
        GridPane.setConstraints(targetLocationLabel, 0, 1);

        TextField targetLocation = new TextField();
        targetLocation.setPromptText("eg.: /user/files/");
        GridPane.setConstraints(targetLocation, 1, 1);


        Label beforeLabel = new Label("Number of lines before Point of Interest");
        GridPane.setConstraints(beforeLabel, 0, 4);

        TextField before = new TextField();
        before.setPromptText("eg.: 3");
        GridPane.setConstraints(before, 1, 4);

        Label afterLabel = new Label("Number of lines after Point of Interest");
        GridPane.setConstraints(afterLabel, 0, 5);

        TextField after = new TextField();
        after.setPromptText("eg.: 3");
        GridPane.setConstraints(after, 1, 5);

        Label saveOptLabel = new Label("Save Option:");
        GridPane.setConstraints(saveOptLabel, 0, 2);

        ChoiceBox<String> saveOpt = new ChoiceBox<>();
        saveOpt.getItems().add("single");
        saveOpt.getItems().add("sep");
        saveOpt.getItems().add("fw");
        saveOpt.getItems().add("screen");
        saveOpt.setValue("single");
        GridPane.setConstraints(saveOpt, 1, 2);


        Label detectorLabel = new Label("Type of Detector:");
        GridPane.setConstraints(detectorLabel, 0, 3);

        CheckBox errBox = new CheckBox("error");
        GridPane.setConstraints(errBox, 1, 3);

        CheckBox excBox = new CheckBox("exception");
        GridPane.setConstraints(excBox, 2, 3);


        help = new Button("Help");
        help.setOnAction(e -> AlertBox.display("HELP",
                helpMessage));
        GridPane.setConstraints(help, 3, 0);


        button = new Button("Analyse");
        GridPane.setConstraints(button, 3, 7);
        button.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * Parses user input and creates a Configuration input according to input.
             * @param event button click
             */
            @Override
            public void handle(ActionEvent event) {
                if (inputFiles.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, grid.getScene().getWindow(), "Form Error!", "Please specify which files to analyse");
                    return;
                }
                if (targetLocation.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, grid.getScene().getWindow(), "Form Error!", "Please choose a location for storing the output files");
                    return;
                }

                if (!errBox.isSelected() && !excBox.isSelected()) {
                    showAlert(Alert.AlertType.ERROR, grid.getScene().getWindow(), "Form Error!", "Please choose at least one analysation criteria");
                    return;
                }
                if (!before.getText().isEmpty() && !before.getText().matches("[0-9]*")) {
                    showAlert(Alert.AlertType.ERROR, grid.getScene().getWindow(), "Form Error!", "Please enter a whole number for before");
                    return;
                }
                if (!after.getText().isEmpty() && !after.getText().matches("[0-9]*")) {
                    showAlert(Alert.AlertType.ERROR, grid.getScene().getWindow(), "Form Error!", "Please enter a whole number for after");
                    return;
                }

                if (!after.getText().isEmpty()) {
                    userAfter = Integer.parseInt(after.getText());

                }
                if (!before.getText().isEmpty()) {
                    userBefore = Integer.parseInt(before.getText());

                }

                userInputFile = inputFiles.getText().split(" ");
                userTarget = targetLocation.getText();
                userSaveOpt = saveOpt.getValue();
                userError = errBox.isSelected();
                userExc = excBox.isSelected();

                Configuration config = new Configuration();
                config.setTargetFolder(userTarget);
                config.setSaveOption(userSaveOpt);
                HashSet<String> files = new HashSet<>(Arrays.asList(userInputFile));
                config.setInputFiles(files);
                config.setWantExceptions(userExc);
                config.setWantErrors(userError);
                config.setBefore(userBefore);
                config.setAfter(userAfter);


                AnalysationDirector.doAnalysation(config);

                //  err  single src/main/resources/test1.log  output/

                showAlert(Alert.AlertType.CONFIRMATION, grid.getScene().getWindow(), "Input parameters correct", "Analysation successfull");
            }
        });


        grid.getChildren().addAll(before, beforeLabel, after, afterLabel, detectorLabel, errBox, excBox, saveOpt, saveOptLabel, inputFiles, inputFilesLabel, button, help, targetLocation, targetLocationLabel);

        Scene scene = new Scene(grid, 650, 300);

        window.setScene(scene);
        window.show();
    }

    /**
     * Closes the GUI
     */
    private void closeProgram() {
        boolean answer = ConfirmationBox.display("Close Program", "Sure you want to exit?");
        System.out.println("Thanks for using log analyser");
        if (answer)
            window.close();
    }

    /**
     * shows Alert
     * @param alertType type of alert, eg help
     * @param owner window
     * @param title title of alter
     * @param message alert message
     */
    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
}

