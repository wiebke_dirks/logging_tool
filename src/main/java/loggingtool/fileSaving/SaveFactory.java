package loggingtool.fileSaving;

import loggingtool.setup.Configuration;

/**
 * Factory class for creating different savers depending on user specification.
 */
public class SaveFactory {
    /**
     * Getter for Saver.
     * @param config Configuration
     * @return type save option user wants
     */
    public Saver getSaver(Configuration config) {

        if (config.getSaveOption().equals("sep")) {
            return new FileSaverSep(config);
        }
        if (config.getSaveOption().equals("fw")) {
            return new FileSaverFilewise(config);
        }
        if (config.getSaveOption().equals("screen")) {
            return new FileSaverScreen(config);
        } else {
            return new FileSaverSingle(config);
        }
    }
}
