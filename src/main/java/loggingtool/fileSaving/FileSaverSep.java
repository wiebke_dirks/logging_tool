package loggingtool.fileSaving;

import loggingtool.Utility;
import loggingtool.fileManagement.FileNameGenerator;
import loggingtool.patterns.Poi;
import loggingtool.setup.Configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Saves all POIs seprarated by type and input file in different output files.
 */
class FileSaverSep extends AbstractFileSaver {
    /**
     * Reference to the location of the folder the POIs are to be saved in.
     */
    private String targetFolder;
    /**
     * Constructor
     *
     * @param config Configuration needed to
     */
    FileSaverSep(Configuration config) {
        super(config);
        this.targetFolder = config.getTargetFolder();
    }

    /**
     * Groups all POIs by type and saves each group in a new file.
     *
     * @param pois to be saved
     */
    public void save(List<Poi> pois) {
        List<Poi> exception = new ArrayList();
        List<Poi> error = new ArrayList();
        //separates exceptions and errors
        for (Poi poi : pois) {
            if (poi.getType().equals("Exception")) {
                exception.add(poi);
            }
            if (poi.getType().equals("Error")) {
                error.add(poi);
            }

        }

        //appends exceptions
        if (!exception.isEmpty()) {
            Utility.sort(exception);

            File file = new File(FileNameGenerator.getInstance().generateNewUniqueFilename(targetFolder));
            appendPoisToFile(file, exception);
        }
        //appends errors
        if (!error.isEmpty()) {
            Utility.sort(error);

            File file = new File(FileNameGenerator.getInstance().generateNewUniqueFilename(targetFolder));
            appendPoisToFile(file, error);
        }
    }
}
