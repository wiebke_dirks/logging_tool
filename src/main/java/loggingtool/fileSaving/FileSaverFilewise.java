package loggingtool.fileSaving;


import loggingtool.fileManagement.FileNameGenerator;
import loggingtool.patterns.Poi;
import loggingtool.setup.Configuration;

import java.io.File;
import java.util.List;


/**
 * Saves all POIs for all detectors for one file in one output file.
 */
class FileSaverFilewise extends AbstractFileSaver {

    /**
     * Reference to the location of the folder the POIs are to be saved in.
     */
    private String targetFolder;

    /**
     * Constructor
     *
     * @param config Configuration needed to
     */
    FileSaverFilewise(Configuration config) {
        super(config);
        this.targetFolder = config.getTargetFolder();
    }

    /**
     * Creates a new file in the output location and saves POIs to this file.
     *
     * @param pois to be saved
     */
    public void save(List<Poi> pois) {

        File file = new File(FileNameGenerator.getInstance().generateNewUniqueFilename(targetFolder));
        appendPoisToFile(file, pois);
    }
}
