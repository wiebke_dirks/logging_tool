package loggingtool.fileSaving;

import loggingtool.Utility;
import loggingtool.fileManagement.FileNameGenerator;
import loggingtool.patterns.Poi;
import loggingtool.setup.Configuration;

import java.io.File;
import java.util.List;

/**
 * Saves all POIs from all input files in just one output file.
 */
class FileSaverSingle extends AbstractFileSaver {
    /**
     * requires reference to the file the pois are saved to
     * so that it can be called multiple times and still
     * append POIs to the same file.
     */
    private final File file;

    /**
     * Constructor. Generates a file with a unique name to be used for appending POIs during saving.
     *
     * @param config Configuration
     */
    FileSaverSingle(Configuration config) {
        super(config);
        file = new File(FileNameGenerator.getInstance().generateNewUniqueFilename(config.getTargetFolder()));
    }

    /**
     * Saves all POIs to same file
     *
     * @param pois to be saved
     */
    public void save(List<Poi> pois) {
        Utility.sort(pois);


        appendPoisToFile(file, pois);

    }
}


