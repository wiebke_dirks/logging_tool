package loggingtool.fileSaving;

import loggingtool.Utility;
import loggingtool.patterns.Poi;
import loggingtool.setup.Configuration;

import java.util.List;

/**
 * Prints all POIs to Standard output.
 */
class FileSaverScreen extends AbstractFileSaver {

    /**Constructor
     * @param config Configuration
     */
    FileSaverScreen(Configuration config) {
        super(config);
    }

    /**
     * Prints all POIs to Standard output.
     * @param pois to be saved
     */
    @Override
    public void save(List<Poi> pois) {
        Utility.sort(pois);
        for (Poi poi : pois) {
            System.out.println(poi.getStringRepresentationOfPoi());
        }

    }
}
