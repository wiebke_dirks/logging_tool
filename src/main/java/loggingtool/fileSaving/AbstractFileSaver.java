package loggingtool.fileSaving;

import loggingtool.patterns.Poi;
import loggingtool.setup.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;

/**
 * Abstract class for saving files.
 */
abstract class AbstractFileSaver implements Saver {

    /**
     * Specifies of which type a Saver is.
     */
    private String type;

    /**
     * Constructor.
     *
     * @param config
     */
    AbstractFileSaver(Configuration config) {
        this.type = config.getSaveOption();
    }

    /**
     * Appends a list of POIs to a file.
     *
     * @param file file that POIs get appended to
     * @param pois append these POIs
     */
    final void appendPoisToFile(File file, List<Poi> pois) {
        try {
            Path filePath = Paths.get(file.getAbsolutePath());

            List<String> toInsert = new LinkedList<>();
            for (Poi poi : pois) {
                toInsert.add(poi.getStringRepresentationOfPoi());
            }
            Files.write(filePath, toInsert, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    /**
     * Abstract method for saving a list of POIs.
     *
     * @param pois to be saved
     */
    public abstract void save(List<Poi> pois);
}
