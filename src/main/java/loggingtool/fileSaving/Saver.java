package loggingtool.fileSaving;

import loggingtool.patterns.Poi;

import java.util.List;

/**
 * Interface for Saver.
 */
public interface Saver {

    /**
     * save list of POIs.
     * @param pois POIs to be saved.
     */
    void save(List<Poi> pois);
}

