package loggingtool.fileManagement;

import loggingtool.detectors.DetectorFactory;
import loggingtool.detectors.PoiDetector;
import loggingtool.patterns.Poi;
import loggingtool.patterns.PoiFuzzyEqualityWrapper;
import loggingtool.setup.Configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class reads from a file and analyses line by line.
 * //TODO add implementation for before and after
 */
public class StreamFileAnalyser {

    /**
     * Path to file that is to be processed.
     */
    private final Path path;


    /**
     * constructor
     *
     * @param path path to file that is to be processed
     */
    public StreamFileAnalyser(final Path path) {
        if (path == null) {
            throw new IllegalArgumentException();
        }
        this.path = path;
    }

    public List<Poi> getPoisFromStreamedFile(Configuration config, Path path) throws IOException {
        try (LineNumberReader br = new LineNumberReader(new FileReader(path.toFile()))) {
            String line;
            DetectorFactory detectorFactory = new DetectorFactory();
            PoiDetector detector = detectorFactory.createDetectorByType(config.getWantedDetectorTypes());
            int count = 0;

            Set<PoiFuzzyEqualityWrapper> pois = new HashSet<>();

            while ((line = br.readLine()) != null) {
                count++;
                if (detector.isPoi(line)) {

                    pois.add(createPoi(detector, line, count, config.getAfter(), br));
                }
            }

            return pois.stream().map(PoiFuzzyEqualityWrapper::getPoi).collect(Collectors.toList());
        }
    }

    private PoiFuzzyEqualityWrapper createPoi(PoiDetector detector, String line, int count, int after, BufferedReader br) {
        String name;
        ArrayList<String> log = new ArrayList<>();

        //add poi
        log.add(line);
        name = line;

        //create new POI object
        Poi poi = new Poi(name, path.toString(), count, log, detector.getName());
        return new PoiFuzzyEqualityWrapper(poi);

    }

}
