package loggingtool.fileManagement;

import java.io.File;


/**
 * Singleton class for a FileNameGenerator that can be used for creating a new file for output every time.
 */
public class FileNameGenerator {
    /**
     * singleton FileNameGenerator.
     */
    private static FileNameGenerator INSTANCE = new FileNameGenerator();

    /**
     * This number is used for naming the new files consecutively
     */
    private int fileCount;

    /**
     * Constructor is made private to ensure no-one can create another FileNameGenerator object
     */
    private FileNameGenerator() {
        // really nothing here
    }

    /**
     * Gets the same instance of the single FileNameGenerator object every time
     *
     * @return instance of FileNameGenerator
     */
    public static FileNameGenerator getInstance() {
        return INSTANCE;
    }

    /**
     * Creates new files with unique names.
     * <p>
     * Generates consequtive file names until it finds a name for afile
     * that does not already exist in the target directory.
     *
     * @return String representation of filepath
     */
    public synchronized String generateNewUniqueFilename(String targetFolder) {

        String path = targetFolder + fileCount + ".txt";

        while (new File(path).exists()) {
            fileCount++;
            path = targetFolder + fileCount + ".txt";
        }
        fileCount++;

        return path;
    }
}
