package loggingtool.fileManagement;

import loggingtool.detectors.DetectorFactory;
import loggingtool.detectors.PoiDetector;
import loggingtool.patterns.Poi;
import loggingtool.setup.Configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains the logic for extracting POIs from a file.
 */
public class FileAnalyser {
    /**
     * Path to file that is to be processed.
     */
    private final Path path;


    /**
     * constructor
     *
     * @param path path to file that is to be processed
     */
    public FileAnalyser(final Path path) {
        if (path == null) {
            throw new IllegalArgumentException();
        }
        this.path = path;
    }

    /**
     * Finds a certain number of lines before occurrence of a POI and adds them to it.
     *
     * @param log        content of POI
     * @param before     number of lines to be added
     * @param countLines line the POI was found
     * @param lines      extracted lines from a log file
     */
    private void addLinesBefore(ArrayList<String> log, int before, int countLines, ArrayList<String> lines) {

        while (before > 0) {
            if (countLines - before > 0) {
                log.add(lines.get(countLines - before));
            }
            before--;
        }
    }

    /**
     * Finds a certain number of lines after occurrence of a POI and adds them to it.
     *
     * @param log        content of POI
     * @param after      number of lines to be added
     * @param countLines line the POI was found
     * @param lines      extracted lines from a log file
     */
    private void addLinesAfter(ArrayList<String> log, int after, int countLines, ArrayList<String> lines) {
        int temp = 1;
        while (temp <= after) {
            if (countLines + temp < lines.size()) {
                log.add(lines.get(countLines + temp));
            }
            temp++;
        }
    }

    /**
     * Returns all POIs from a file with corresponding name of type of POI
     *
     * @param config specific Configuration
     * @param path   path to file that the POIs are to be extracted from
     * @return POIs with corresponding name of detector
     * @throws IOException if IOException occurred
     */
    public List<Poi> getAllPoisFromFile(Configuration config, String path) throws IOException {
        FileAnalyser fp = new FileAnalyser(Paths.get(path));

        ArrayList<String> lines = getLinesFromFile(path);

        DetectorFactory detectorFactory = new DetectorFactory();
        PoiDetector detector = detectorFactory.createDetectorByType(config.getWantedDetectorTypes());

        return fp.getPoi(lines, detector, config.getBefore(), config.getAfter());
    }

    /**
     * Returns POIs for a certain detector.
     *
     * @param lines    all lines from a file
     * @param detector that is to be used for analysing
     * @param before   how many lines before POI should be added
     * @param after    how many lines after POI should be added
     * @return all POIs for a certain detector
     */
    private List<Poi> getPoi(ArrayList<String> lines, PoiDetector detector, int before, int after) {

        final List<Poi> pois = new LinkedList<>();

        int countLines = 0;
        int poiAtLine;

        for (final String line : lines) {
            String name;

            if (line != null && detector.isPoi(line)) {

                ArrayList<String> log = new ArrayList<>();


                //add lines before error
                addLinesBefore(log, before, countLines, lines);

                //add poi
                log.add(line);
                name = line;
                poiAtLine = countLines + 1;

                int addToCount = 0;
                if (detector.getName().equals("Exception")) {
                    int logSizeBefore = log.size();
                    multipleLinesPoi(log, countLines, lines);
                    addToCount = log.size() - logSizeBefore;
                }

                addLinesAfter(log, after, countLines + addToCount, lines);

                // new POI object
                final Poi poi = new Poi(name, path.toString(), poiAtLine, log, detector.getName());
                pois.add(poi);

            }
            countLines++;

        }
        return pois;
    }

    /**
     * Used for POIs that span multiple lines to add these lines.
     *
     * @param log        of the POI
     * @param countLines needs to be incremented
     * @param lines      all lines from file
     */
    private void multipleLinesPoi(ArrayList<String> log, int countLines, ArrayList<String> lines) {

        countLines++;

        while (countLines < lines.size() && lines.get(countLines).matches("^!.*")) {
            log.add(lines.get(countLines));
            countLines++;
        }
    }

    /**
     * This method retrieves the individual lines from an input file.
     *
     * @param filePath for file from which we want the content
     * @return individual lines of content as Array
     * @throws IOException if IOException occurred
     */
    private ArrayList<String> getLinesFromFile(String filePath) throws IOException {

        ArrayList<String> output = new ArrayList<>();
        FileReader fr = new FileReader(filePath);
        BufferedReader br = new BufferedReader(fr);

        String line = br.readLine();

        while (line != null) {
            output.add(line);
            line = br.readLine();
        }
        return output;
    }

}
