package loggingtool;

import loggingtool.patterns.Poi;

import java.util.List;

/**
 * Utility class.
 */
public class Utility {

    /**
     * Sorts a POIs so that they are in order of appearance.
     *
     * @param result unsorted POIs
     * @return sorted list of POIs
     */
    public static List<Poi> sort(List<Poi> result) {
        result.sort((poi1, poi2) -> {
            if (poi1.getStartLine() == poi2.getStartLine()) {
                return 0;
            }
            return poi1.getStartLine() < poi2.getStartLine() ? -1 : 1;
        });
        return result;
    }
}
