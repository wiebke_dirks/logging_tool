package loggingtool;

import javafx.application.Application;
import loggingtool.setup.ArgumentParser;
import loggingtool.setup.ConfigException;
import loggingtool.setup.Configuration;
import loggingtool.setup.ui.ui;

/**
 *  Analyses logging files according to the specification of a user.
 *  Extracts Points Of Interests (POI) from theses files.
 *  Organises these POIs and saves them in text files or prints them to Standard Output.
 *
 */
class Main {
    /**
     * If user specified arguments via command line, these arguments are parsed.
     * Otherwise the GUI is started to assist user in choosing parameters.
     *
     * @param args user specification for analysing process
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            ArgumentParser parser = new ArgumentParser();

            Configuration config = new Configuration();

            // try to parser the options a user has specified and stores them
            // in a Configuration object.
            try {
                config = parser.parseInput(args);
            } catch (ConfigException e) {
                e.printStackTrace();
                System.exit(1);
            }
            AnalysationDirector.doAnalysation(config);

        } else {
            Application.launch(ui.class, args);
        }
    }
}
