package loggingtool.detectors;

/**
 * This class represents a detector that only searches for errors
 */
class ErrorPoiDetector implements PoiDetector {


    // FIELDS
    /**
     * name of the detector. needed to show in output files what detector found the line of interest
     */
    private String name;


    // CONSTRUCTORS

    /**
     * default constructor
     */
    ErrorPoiDetector() {
        this.name = "Error";
    }

    /**
     * getter for name of constructor
     *
     * @return name of constructor
     */
    public String getName() {
        return name;
    }

    /**
     * Analyses if a line contains an error.
     *
     * @param line line to be analysed
     * @return whether a line is a Point of Interest.
     */
    @Override
    public boolean isPoi(String line) {
        return line.matches("^ERROR.*");
    }
}
