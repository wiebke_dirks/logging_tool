package loggingtool.detectors;


/**
 * This class represents a detector that only searches for exceptions
 */
class ExceptionPoiDetector implements PoiDetector {

    /**
     * name of the detector. needed to show in output files what detector found the line of interest
     */
    private String name;

    /**
     * default constructor
     */
    ExceptionPoiDetector() {
        this.name = "Exception";
    }

    /**
     * getter for name of constructor
     *
     * @return name of constructor
     */
    public String getName() {
        return name;
    }

    /**
     * Analyses if a line contains an exception.
     *
     * @param line line to be analysed
     * @return whether a line is a Point of Interest.
     */
    @Override
    public boolean isPoi(String line) {
        return line.matches(".*Exception: .*") &&
                !line.contains("INFO") && line.matches("^!.*") &&
                !line.matches("^! Caused by.*");
    }
}
