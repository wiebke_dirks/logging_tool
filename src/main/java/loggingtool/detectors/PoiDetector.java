package loggingtool.detectors;


/**
 * A detector for a point of interest.
 */
public interface PoiDetector {

    boolean isPoi(String line);

    String getName();
}
