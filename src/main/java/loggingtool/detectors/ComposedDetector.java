package loggingtool.detectors;

import java.util.LinkedList;
import java.util.List;

/**
 * A composed detector consists of one or more detectors.
 */
class ComposedDetector implements PoiDetector {
    /**
     * List of detectors the composed detector is made of.
     */
    private final List<PoiDetector> detectors;
    /**
     * Name of the POI
     */
    private String name;

    ComposedDetector(List<PoiDetector> detectors) {
        this.detectors = new LinkedList<>(detectors);
        this.name = "Point of interest";
    }


    /**
     * A line is a POI if one of the detectors that make up the composed detectors
     * identifies the line to be a POI.
     *
     * @param line input
     * @return whether line is a POI.
     */
    @Override
    public boolean isPoi(String line) {

        for (final PoiDetector detector : detectors) {

            if (detector.isPoi(line)) {
                name = detector.getName();
                return true;
            }
        }
        return false;
    }

    /**
     * Getter for name of POI.
     *
     * @return name
     */
    public String getName() {
        return name;
    }
}
