package loggingtool.detectors;

import java.util.LinkedList;
import java.util.List;

/**
 * Factory class to create detectors.
 */
public class DetectorFactory {

    private List<PoiDetector> getDetectorList(DetectorType... types) {
        List<PoiDetector> poiDetectorList = new LinkedList<>();
        for (final DetectorType type : types) {
            poiDetectorList.add(createDetectorByTypeInternal(type));
        }
        return poiDetectorList;
    }

    private PoiDetector createDetectorByTypeInternal(DetectorType type) {

        switch (type) {
            case EXCEPTION:
                return new ExceptionPoiDetector();
            case ERROR:
                return new ErrorPoiDetector();
            default:
                // This happens when someone adds new type and forget to handle it here.
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }

    /**
     * Creates a new ComposedDetector
     *
     * @param types detectors user wants to use for analysation
     * @return detector
     */
    public PoiDetector createDetectorByType(DetectorType... types) {
        return new ComposedDetector(getDetectorList(types));
    }

    /**
     * All individual detectors available.
     */
    public enum DetectorType {
        EXCEPTION,
        ERROR
    }
}
