package loggingtool.patterns;

import org.junit.Assert;
import org.junit.Test;


public class EditingDistanceTest {

    @Test
    public void testEditingDistance(){
        String str1 = "Hello world";
        String str2 = "ello Mold";
        int actual = EditingDistance.getEditingDistance(str1,str2);

        Assert.assertEquals(3,actual);
    }


    @Test
    public void testInsert(){
        String str1 = "Hello world";
        String str2 = "ello world";
        int actual = EditingDistance.getEditingDistance(str1,str2);

        Assert.assertEquals(1,actual);
    }

    @Test
    public void testReplace(){
        String str1 = "Hello world";
        String str2 = "Gello world";
        int actual = EditingDistance.getEditingDistance(str1,str2);

        Assert.assertEquals(1,actual);
    }

    @Test
    public void testDelete(){
        String str1 = "Hello worldd";
        String str2 = "Hello world";
        int actual = EditingDistance.getEditingDistance(str1,str2);

        Assert.assertEquals(1,actual);
    }




}