package loggingtool.patterns;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UniquenessEvaluatorTest {


    @Test
    public void testGetUniquePoisForDifferentPois(){
        ArrayList<String> log1 = new ArrayList<>();
        log1.add("poinumber1");
        log1.add("line2");
        ArrayList<String> log2 = new ArrayList<>();
        log2.add("poinumber2isverydifferent");
        log2.add("line2");
        Poi poi1 = new Poi("poinumber1", "test",12, log1 ,"Error");
        Poi poi2 = new Poi("poinumber2isverydifferent", "test", 13, log2, "Error");

        List<Poi> poiList = new ArrayList<>();
        poiList.add(poi1);
        poiList.add(poi2);

        List<Poi> uniquePois = UniquenessEvaluator.getUniquePois(poiList);

        Assert.assertTrue(uniquePois.size()==2);

    }
    @Test
    public void testGetUniquePoisForSamePois(){
        ArrayList<String> log1 = new ArrayList<>();
        log1.add("poinumber1");
        log1.add("line2");
        ArrayList<String> log2 = new ArrayList<>();
        log2.add("poinumber1");
        log2.add("line2");
        Poi poi1 = new Poi("poinumber1", "test",12, log1 ,"Error");
        Poi poi2 = new Poi("poinumber1", "test", 12, log2, "Error");

        List<Poi> poiList = new ArrayList<>();
        poiList.add(poi1);
        poiList.add(poi2);
        List<Poi> uniquePois = UniquenessEvaluator.getUniquePois(poiList);

        Assert.assertTrue(uniquePois.size()==1);
    }

}