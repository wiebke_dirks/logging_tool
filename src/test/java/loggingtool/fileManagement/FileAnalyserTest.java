package loggingtool.fileManagement;

import loggingtool.Utility;
import loggingtool.patterns.Poi;
import loggingtool.setup.ArgumentParser;
import loggingtool.setup.ConfigException;
import loggingtool.setup.Configuration;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class FileAnalyserTest {




    @Test
    public void testGetAllPoisFromFileForErrors() throws ConfigException, IOException {

        String path = "src/test/resources/loggingtool/fileManagement/test01.log";

        String[] args = new String[]{ "-outputFormat", "single", "-err",  "-p",  path};

        ArgumentParser parser = new ArgumentParser();

        Configuration config = parser.parseInput(args);



        FileAnalyser analyser = new FileAnalyser(Paths.get(path));


        List<Poi> findings = analyser.getAllPoisFromFile(config, path);
        int numberOfPois = findings.size();


        Assert.assertEquals(1, numberOfPois);
    }
    @Test
    public void testGetAllPoisFromFileForExceptions() throws ConfigException, IOException {


        String path = "src/test/resources/loggingtool/fileManagement/test01.log";

        String[] args = new String[]{ "-outputFormat", "single", "-exc",  "-p",  path};

        ArgumentParser parser = new ArgumentParser();

        Configuration config = parser.parseInput(args);

        FileAnalyser analyser = new FileAnalyser(Paths.get(path));

        List<Poi> findings = analyser.getAllPoisFromFile(config, path);
        int numberOfPois = findings.size();


        Assert.assertEquals(2, numberOfPois);
    }
    @Test
    public void testGetAllPoisFromFileForExceptionsAndErrors() throws ConfigException, IOException {


        String path = "src/test/resources/loggingtool/fileManagement/test01.log";

        String[] args = new String[]{ "-outputFormat", "single", "-err", "-exc",  "-p",  path};

        ArgumentParser parser = new ArgumentParser();

        Configuration config = parser.parseInput(args);

        FileAnalyser analyser = new FileAnalyser(Paths.get(path));

        List<Poi> findings = analyser.getAllPoisFromFile(config, path);
        int numberOfPois = findings.size();


        Assert.assertEquals(3, numberOfPois);
    }

    @Test
    public void testAddLinesAfter() throws ConfigException, IOException{
        String path = "src/test/resources/loggingtool/fileManagement/test01.log";
        String[] args = new String[]{ "-outputFormat", "single", "-err", "-a", "1",  "-p",  path};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        FileAnalyser analyser = new FileAnalyser(Paths.get(path));
        List<Poi> findings = analyser.getAllPoisFromFile(config, path);


        int count = 0;
            for (Poi poi : findings) {
                String line = poi.getStringRepresentationOfPoi();
                count = line.length() - line.replace("\n", "").length();
            }

        Assert.assertEquals(4,count);
    }

    @Test
    public void testAddLinesBefore() throws ConfigException, IOException{
        String path = "src/test/resources/loggingtool/fileManagement/test01.log";
        String[] args = new String[]{ "-outputFormat", "single", "-err", "-b", "2",  "-p",  path};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        FileAnalyser analyser = new FileAnalyser(Paths.get(path));
        List<Poi> findings = analyser.getAllPoisFromFile(config, path);

        int count = 0;
            for (Poi poi : findings) {
                String line = poi.getStringRepresentationOfPoi();
                count = line.length() - line.replace("\n", "").length();
            }
        Assert.assertEquals(5,count);
    }
}