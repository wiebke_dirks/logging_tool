package loggingtool.fileManagement;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class FileCreatorTest {

    @Test
    public void instanceIsNotNull() {
        Assert.assertNotNull(FileNameGenerator.getInstance());
    }

    @Test
    public void sameInstanceIsReturnedByMultipleGetInstanceCalls() {
        Assert.assertSame(
                FileNameGenerator.getInstance(),
                FileNameGenerator.getInstance()
        );
    }

    @Test
    public void testFoo() throws IOException {
        InputStream is = getClass().getResourceAsStream("foo.txt");
        Assert.assertNotNull(is);
        String line = new BufferedReader(new InputStreamReader(is)).readLine();
        Assert.assertEquals("bar", line);
    }

//    @Test
//    public void testCreateFile(){
//        File newFile = new File(FileNameGenerator.getInstance().generateNewUniqueFilename());
//        Assert.assertNotNull(newFile);
//    }
    @Test
    public void testUniqueFiles(){
        File newFile1 = new File(FileNameGenerator.getInstance().generateNewUniqueFilename("test/"));
        File newFile2 = new File(FileNameGenerator.getInstance().generateNewUniqueFilename("test/"));
        Assert.assertNotSame(newFile1,newFile2);
    }

    // is now private so cannot test
//    @Test
//    public void namesAreUnique() {
//        final Set<String> names = new HashSet<>();
//        for (int i = 0; i < 100; i++) {
//            names.add(FileNameGenerator.getInstance().generateNewUniqueFilepath());
//        }
//        Assert.assertEquals(100, names.size());
//    }

}