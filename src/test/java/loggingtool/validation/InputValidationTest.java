package loggingtool.validation;

import loggingtool.setup.ArgumentParser;
import loggingtool.setup.ConfigException;
import loggingtool.setup.Configuration;
import org.junit.Assert;
import org.junit.Test;

public class InputValidationTest {



    @Test(expected = IllegalArgumentException.class)
    public void testThatNullConfigIsRefused() {
        new InputValidation(null);
    }

//    @Before
//    public void setUp() {
//
//        String[] args = new String[]{ "-outputFormat", "single", "-err",   "-exc",  "-b", "2",  "-a", "3",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
//        config = new Configuration(args);
//    }


    @Test
    public void testCorrectInput() throws ConfigException {
        String[] args = new String[]{ "-outputFormat", "single", "-err", "-target", "target/out/",  "-exc",  "-b", "2",  "-a", "3",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertTrue(good);
    }


    @Test
    public void testNegativeBefore() throws ConfigException{
        String[] args = new String[]{ "-outputFormat", "fw",   "-exc", "-a", "3", "-b", "-2",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        ArgumentParser parser = new ArgumentParser();        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }

    @Test
    public void testNegativeAfter() throws ConfigException{
        String[] args = new String[]{ "-outputFormat", "fw",   "-exc", "-a", "-3", "-b", "2",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        ArgumentParser parser = new ArgumentParser();        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }
    @Test
    public void testNoAnalysationCriteria() throws ConfigException{
        String[] args = new String[]{ "-outputFormat", "fw", "-a", "3", "-b", "2",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        ArgumentParser parser = new ArgumentParser();        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }
    @Test
    public void testNoSaveOption() throws ConfigException{
        String[] args = new String[]{ "-err", "-a", "3", "-b", "2",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        ArgumentParser parser = new ArgumentParser();        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }
    @Test
    public void testNoValidSaveOption() throws ConfigException{
        String[] args = new String[]{ "-err", "-out", "nonsense", "-a", "3", "-b", "2",  "-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        ArgumentParser parser = new ArgumentParser();        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }
    @Test
    public void testEmptyInputFiles() throws ConfigException{
        String[] args = new String[]{ "-outputFormat", "single", "-err",   "-exc",  "-b", "2",  "-a", "3", "-p", ""};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }
    @Test
    public void testMissingInputFiles() throws ConfigException{
        String[] args = new String[]{ "-outputFormat", "single", "-err",   "-exc",  "-b", "2",  "-a", "3"};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        boolean good = new InputValidation(config).checkUsage();
        Assert.assertFalse(good);
    }

}