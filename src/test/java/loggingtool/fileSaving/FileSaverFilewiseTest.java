package loggingtool.fileSaving;

import loggingtool.fileManagement.FileAnalyser;
import loggingtool.patterns.Poi;
import loggingtool.setup.ArgumentParser;
import loggingtool.setup.ConfigException;
import loggingtool.setup.Configuration;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class FileSaverFilewiseTest {
    @Test
    public void save() throws ConfigException, IOException {

        String path1 = "src/test/resources/loggingtool/fileManagement/test01.log";
        String path2 = "src/test/resources/loggingtool/fileManagement/test02.log";

        String outputFolder = "src/test/testOutput/";
        int countBefore = new File(outputFolder).listFiles().length;
        String[] args = new String[]{ "-outputFormat", "fw", "-err", "-a", "1",  "-p",  path1, path2, "-target", outputFolder};

        SaveFactory saveFactory = new SaveFactory();
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Saver saver = saveFactory.getSaver(config);

        for (final String path : config.getInputFiles()) {

            FileAnalyser analyser = new FileAnalyser(Paths.get(path));
            List<Poi> findings = analyser.getAllPoisFromFile(config, path);
            saver.save(findings);
        }

        int countAfter = new File(outputFolder).listFiles().length;

        Assert.assertEquals(2, countAfter - countBefore);

    }
}