package loggingtool.fileSaving;

import loggingtool.fileManagement.FileAnalyser;
import loggingtool.patterns.Poi;
import loggingtool.setup.ArgumentParser;
import loggingtool.setup.ConfigException;
import loggingtool.setup.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class FileSaverScreenTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }
    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testPrintToScreen() throws ConfigException, IOException {

        String path1 = "src/test/resources/loggingtool/fileManagement/test01.log";
        String outputFolder = "src/test/testOutput/";

        String[] args = new String[]{ "-outputFormat", "screen", "-err",  "-p",  path1, "-target", outputFolder};

        SaveFactory saveFactory = new SaveFactory();
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Saver saver = saveFactory.getSaver(config);


        for (final String path : config.getInputFiles()) {

            FileAnalyser analyser = new FileAnalyser(Paths.get(path));
            List<Poi> findings = analyser.getAllPoisFromFile(config, path);

            saver.save(findings);

        }


        Assert.assertEquals("Input file: src/test/resources/loggingtool/fileManagement/test01.log\n" +
                "Error found at line: 5\n" +
                "ERROR 5\n\n", outContent.toString());



    }
}
