package loggingtool.setup;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class ArgumentParserTest {



    @Test
    public void testAfterPositive() throws ConfigException{
        String[] args = new String[]{  "-a", "3"};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Assert.assertEquals(3,config.getAfter());
    }
    @Test(expected = ConfigException.class)
    public void testAfterNAN() throws ConfigException{
        String[] args = new String[]{  "-a", "nonsense"};
        ArgumentParser parser = new ArgumentParser();
        parser.parseInput(args);
    }
    @Test
    public void testAfterDefault() throws ConfigException{
        String[] args = new String[]{};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Assert.assertEquals(0,config.getAfter());
    }

    @Test
    public void testBeforePositive() throws ConfigException{
        String[] args = new String[]{  "-b", "3"};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Assert.assertEquals(3,config.getBefore());
    }
    @Test(expected = ConfigException.class)
    public void testBeforeNAN() throws ConfigException{
        String[] args = new String[]{  "-b", "nonsense"};
        ArgumentParser parser = new ArgumentParser();
        parser.parseInput(args);
    }
    @Test
    public void testBeforeDefault() throws ConfigException{
        String[] args = new String[]{};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Assert.assertEquals(0,config.getBefore());
    }

    @Test
    public void testInputFiles() throws ConfigException{
        String[] args = new String[]{"-p",  "src/main/resources/test1.log",  "src/main/resources/test2.log"};
        Set<String> inputs = new HashSet<>();
        inputs.add("src/main/resources/test1.log");
        inputs.add("src/main/resources/test2.log");
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Assert.assertEquals(inputs,config.getInputFiles());
    }
    @Test(expected = ConfigException.class)
    public void testMissingInputFiles() throws ConfigException{
        String[] args = new String[]{"-p"};
        ArgumentParser parser = new ArgumentParser();
        parser.parseInput(args);
    }
    @Test
    public void testSaveOption() throws ConfigException{
        String[] args = new String[]{  "-out", "fw"};
        ArgumentParser parser = new ArgumentParser();
        Configuration config = parser.parseInput(args);
        Assert.assertEquals("fw",config.getSaveOption());
    }
    @Test(expected = ConfigException.class)
    public void testSaveOptionNull() throws ConfigException{
        String[] args = new String[]{  "-out"};
        ArgumentParser parser = new ArgumentParser();
        parser.parseInput(args);

    }
}